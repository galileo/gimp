;################################################################################
; source        : répertoire des images à modifier
; listefichiers : noms des fichiers jpeg à traiter
; nomfichier    : nom du fichier courant
; image         : objet image 
; calque        : calque actif 
; destination   : répertoire où enregistrer les images
; qualite       : qualité de la compression 
;
; Auteur        : Nordine VALLAS - 3 Novembre 2020
; Licence       : GPL3
;#################################################################################

( define (jpegCompress source destination qualite) 
	( let* ((listefichiers (cadr (file-glob (string-append source "\\*.jpg") 1))))	
		( while (not (null? listefichiers))	
			( let* 
				(
					(nomfichier (car listefichiers))	 
					(image (car (gimp-file-load RUN-NONINTERACTIVE nomfichier nomfichier)))	
					(calque (car (gimp-image-get-active-layer image)))	
					(destination (string-append destination "\\" (car (gimp-image-get-name image))))	
				) 
				
				(file-jpeg-save RUN-NONINTERACTIVE image calque destination destination qualite 0 1 0 "" 2 1 0 0)
				
				(gimp-image-delete image)
			
			)
			(set! listefichiers (cdr listefichiers)) 
		)
	)
)

(
	script-fu-register "jpegCompress" 
	"<Image>/DDT72/jpegCompress" 
	 "-------------------------------"
	 "Converti tous toutes les images d'un répertoire avec qualité de compression donnée"
	 "Nordine VALLAS"
	 "202020"
	 "-------------------------------"
	SF-DIRNAME "Source"  "c:/source"	
	SF-DIRNAME "Destination"  "c:/destination"
	SF-ADJUSTMENT "Compression" '( 0.8 0 1 0.1 0.2 1 0 )
)  
